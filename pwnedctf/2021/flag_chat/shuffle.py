f1_orders = [0x0d, 0x02, 0x0b, 0x13, 0x1b, 0x09, 0x0a, 0x00, 0x10, 0x06, 0x07, 0x1a, 0x05, 0x12, 0x04, 0x19, 0x11, 0x0e, 0x17, 0x16, 0x0f, 0x1c, 0x1d, 0x18, 0x08, 0x15, 0x01, 0x03, 0x1f, 0x0c, 0x1e, 0x14]

f2_offset_mem = [0xa8, 0x5f, 0x43, 0xdf, 0x90, 0x15, 0xa2, 0xf5, 0x77, 0x48, 0x49, 0x6c, 0x67, 0x20, 0x0e, 0xcd, 0xb6, 0xc8, 0x4a, 0xe7, 0x89, 0x2f, 0xa1, 0xa6, 0xe8, 0xb7, 0xe1, 0xc6, 0x58, 0xa9, 0xd4, 0x5a, 0x4d, 0x9e, 0x34, 0x05, 0x53, 0xc2, 0x76, 0xd3, 0xc5, 0xb3, 0xbf, 0xc9, 0xaf, 0x98, 0x25, 0x68, 0xd9, 0x2d, 0xe6, 0x65, 0xd7, 0x59, 0xd6, 0x0a, 0x31, 0x8f, 0x99, 0xaa, 0x7c, 0xc0, 0x35, 0xb5, 0xed, 0x4b, 0xeb, 0xd5, 0x8e, 0x6b, 0x9d, 0x37, 0x2e, 0x62, 0x0f, 0x07, 0x9b, 0x87, 0xb8, 0xbd, 0xde, 0x69, 0xc7, 0xcf, 0x66, 0x46, 0x60, 0x04, 0xd0, 0xa7, 0xf8, 0x70, 0x7e, 0xfa, 0x9a, 0x03, 0x08, 0xc4, 0xf6, 0x8b, 0x79, 0x33, 0x23, 0xdd, 0xda, 0xc1, 0x13, 0xce, 0x16, 0xee, 0x93, 0x63, 0x12, 0x6f, 0x83, 0x0d, 0x71, 0x64, 0x4c, 0x51, 0x00, 0xba, 0xef, 0x95, 0x6e, 0x22, 0xe5, 0x94, 0x30, 0xfb, 0x14, 0x41, 0x7a, 0x1c, 0x2a, 0x56, 0xb9, 0x38, 0x42, 0xf0, 0x44, 0xf3, 0xf2, 0x9f, 0x52, 0x4e, 0xd8, 0xcb, 0x24, 0x32, 0xbe, 0x0c, 0xa3, 0x09, 0x85, 0x01, 0x1d, 0xa5, 0x28, 0x45, 0xf4, 0x47, 0xcc, 0xae, 0xc3, 0xab, 0xa0, 0x92, 0x72, 0x57, 0xac, 0x3e, 0xe3, 0xb4, 0x74, 0x1b, 0x81, 0x4f, 0xdc, 0x2b, 0x50, 0x02, 0x27, 0xb2, 0x6d, 0xf1, 0x54, 0xfe, 0x80, 0x5e, 0x3b, 0x36, 0xe2, 0xff, 0x11, 0xea, 0xfd, 0x1a, 0x97, 0x86, 0x26, 0x73, 0xb1, 0xd2, 0x3a, 0x1e, 0x5d, 0x39, 0x7f, 0x1f, 0xa4, 0x91, 0x5c, 0x55, 0xec, 0xe4, 0x29, 0x8c, 0xf7, 0x7d, 0x18, 0x82, 0xbc, 0x2c, 0x75, 0x40, 0xbb, 0x17, 0x8d, 0xf9, 0xd1, 0xe9, 0x0b, 0x7b, 0x10, 0xca, 0x6a, 0xfc, 0x19, 0x3c, 0x8a, 0xb0, 0xad, 0x21, 0x96, 0x5b, 0x06, 0x61, 0x3d, 0x3f, 0x88, 0x78, 0xdb, 0x84, 0x9c, 0xe0]

f3_bytes = [0x42, 0x33, 0x21, 0x68]

base = [0x50, 0x21, 0x50, 0xeb, 0x86, 0xb0, 0x44, 0x65, 0x4f, 0x3e, 0x44, 0x0d, 0x41, 0xea, 0xa2, 0xeb, 0x13, 0xe4, 0xb2, 0x0c, 0x4f, 0xfd, 0xf6, 0x9e, 0xc9, 0x30, 0x45, 0x0d, 0x54, 0x30, 0xd7, 0x11]

flag_resource = [0x03, 0x02, 0x1e, 0x56, 0x16, 0x24, 0x47, 0x5f, 0x0e, 0x42, 0x40, 0x00, 0x00, 0x1c, 0x05, 0x18, 0x40, 0x28, 0x56, 0x10, 0x3b, 0x32, 0x46, 0x1d, 0x11, 0x41, 0x5d, 0x06, 0x58, 0x46, 0x43, 0x11]

def f3_solve(passwd):
    new_passwd = []

    for i in range(0, len(passwd)):
        ind_mod = f3_bytes[i % 4]
        new_passwd.append(ind_mod ^ passwd[i])

    return new_passwd

def f2_solve(passwd):
    new_passwd = []

    for pb in passwd:
        for mm in range(0, len(f2_offset_mem)):
            if pb == f2_offset_mem[mm]:
                new_passwd.append(mm)
                break

    return new_passwd

def f1_solve(passwd):
    new_passwd = []

    for ind in range(0, len(f1_orders)):
        for on in range(0, len(f1_orders)):
            if f1_orders[on] == ind:
                new_passwd.append(passwd[on])
                break

    return new_passwd

def get_flag(passwd):
    flag = []

    for i in range(0, len(flag_resource)):
        flag.append(flag_resource[i] ^ passwd[i])

    return flag

def arr_to_str(arr):
    string = ""

    for i in arr:
        string += chr(i)

    return string

print(arr_to_str(get_flag(f1_solve(f2_solve(f3_solve(base))))))
