# FlagChat Challenge

__date__: 27-28/02/2021

__author__: ShinyJonny

__CTF__: PwnEd CTF

__challenge__: FlagChat

__challenge author__: xenocidewiki

## Initial observation

We are presented with a file named `server` and the description: `I managed to steal a binary running someone's server, but can still not figure out how to authenticate with it. Mind helping me out?`.

When we execute the command `file` on this file, we find out that it is an x86\_64, stripped, linux ELF executable.

So, we open it in radare2. (You can use IDA the roguelike, or any other dissassembly/binary analysis tool)

We display the binary info and find out that it is written in C (GCC), and other info that is not relevant for this challenge. (mainly `nx`, `stripped`, `pic`)

## Analysis

We print the disassembly of the entrypoint.
```gdb
┌ 46: entry0 (int64_t arg3);
│           ; arg int64_t arg3 @ rdx
│           0x004010f0      f30f1efa       endbr64                     ; [13] -r-x section size 2325 named .text
│           0x004010f4      31ed           xor ebp, ebp
│           0x004010f6      4989d1         mov r9, rdx                 ; arg3
│           0x004010f9      5e             pop rsi
│           0x004010fa      4889e2         mov rdx, rsp
│           0x004010fd      4883e4f0       and rsp, 0xfffffffffffffff0
│           0x00401101      50             push rax
│           0x00401102      54             push rsp
│           0x00401103      49c7c0001a40.  mov r8, 0x401a00
│           0x0040110a      48c7c1901940.  mov rcx, 0x401990
│           0x00401111      48c7c7501940.  mov rdi, main               ; 0x401950
└           0x00401118      ff15d22e0000   call qword [reloc.__libc_start_main] ; [0x403ff0:8]=0
            0x0040111e      f4             hlt
```
Nothing unusual is happening at the startup (except for the end looking like it is patched), so let's go straight to the main function.

The main function is very simple: a function at `0x401510` is called, and if the return value is not equal to 0, another function at `0x4015c0` is called.
```gdb
│           0x0040195f      e8acfbffff     call fcn.00401510
│           0x00401964      83f800         cmp eax, 0
│       ┌─< 0x00401967      0f8405000000   je 0x401972
│       │   0x0040196d      e84efcffff     call fcn.004015c0
```

Let's look at the first function (`fcn.00401510`)

A new tcp/ip4 socket is created with `call sym.imp.socket`, and the fd is saved to global variable `0x40425c`. Let's call this variable `sock1`.
```gdb
│           0x00401518      31d2           xor edx, edx                ; int protocol
│           0x0040151a      bf02000000     mov edi, 2                  ; int domain
│           0x0040151f      be01000000     mov esi, 1                  ; int type
│           0x00401524      e8b7fbffff     call sym.imp.socket         ; int socket(int domain, int type, int protocol)
│           0x00401529      8904255c4240.  mov dword [0x40425c], eax   ; [0x40425c:4]=0
```

If that did not fail, a the socket is bound to the port `13337`.
```gdb
│      │└─> 0x0040154a      66c704251042.  mov word [0x404210], 2      ; [0x404210:2]=0
│      │    0x00401554      c70425144240.  mov dword [0x404214], 0     ; [0x404214:4]=0
│      │    0x0040155f      bf19340000     mov edi, 0x3419
│      │    0x00401564      e8f7faffff     call sym.imp.htons
│      │    0x00401569      48b910424000.  movabs rcx, 0x404210
│      │    0x00401573      668904251242.  mov word [0x404212], ax     ; [0x404212:2]=0
│      │    0x0040157b      8b3c255c4240.  mov edi, dword [0x40425c]   ; [0x40425c:4]=0 ; int socket
│      │    0x00401582      4889ce         mov rsi, rcx                ; struct sockaddr*address
│      │    0x00401585      ba10000000     mov edx, 0x10               ; 16 ; socklen_t address_len
│      │    0x0040158a      e811fbffff     call sym.imp.bind           ; int bind(int socket, struct sockaddr*address, socklen_t address_len)

```

Let's name this whole function `fcn.bind_socket`.

Now, let's see the second function of main. The function is quite large, so we jump to the visual mode and view the graph (here displayed by cutter).
![fcn.main_loop graph](img/fcn.main_loop.png)

It looks quite intimidating, but actually is very simple.
It looks like the main loop of the server, so we will call it `fcn.main_loop`.

The function can be divided into several parts, the important ones being:

1. __Accept conn__ - accept a connection. (fork)
2. __Recieve__ - recieve 100 bytes.
3. __Command switch__ - parse the input for commands.
4. __Gate__ - jump using a jump table.
5. __Again__ - send error msg and return to __Recieve__.
6. __Exit__ - Return from the function.

![graph annotated](img/annotated_graph.png)

### Accept conn

First, a connection is accepted. If it failed, Go to __Exit__. (kill the process fork)
If successfull, go to __Recieve__.

### Recieve

0 is saved into a global variable at `0x404244`. We will call this variable `option`.

100 bytes are recieved and saved to `buffer`. (`ebp-0x70`)
```gdb
│    ╎╎│╎   0x00401662      488d7590       lea rsi, [buffer]           ; void *buffer
│    ╎╎│╎   0x00401666      c70425444240.  mov dword [0x404244], 0     ; [0x404244:4]=0
│    ╎╎│╎   0x00401671      c7458c000000.  mov dword [var_74h], 0
│    ╎╎│╎   0x00401678      8b3c25584240.  mov edi, dword [0x404258]   ; [0x404258:4]=0 ; int socket
│    ╎╎│╎   0x0040167f      ba64000000     mov edx, 0x64               ; 'd' ; 100 ; size_t length
│    ╎╎│╎   0x00401684      e8a7f9ffff     call sym.imp.recv           ; ssize_t recv(int socket, void *buffer, size_t length, int flags)
```

This is looped until some bytes are read.
Then, we jump to the __Command switch__.

### Command switch

Here, the `buffer` is searched for 7 different strings:

- `!hello`
- `!secret!`
- `!password`
- `!flag`
- `!ping`
- `!cofee`
- `!quit`

A match results in moving values from 1-6 into `option`, except for `!quit`, which immediately jumps to __Exit__.

At the end of the switch, if `option` remains 0, (nothing found) the code jumps to __Again__. Else, it proceeds to the __Gate__.
```gdb
│   └└────> 0x0040180a      8b05342a0000   mov eax, dword [0x00404244] ; [0x404244:4]=0
│     ╎│╎   0x00401810      83c0ff         add eax, 0xffffffff
│     ╎│╎   0x00401813      89c1           mov ecx, eax
│     ╎│╎   0x00401815      83e805         sub eax, 5
│     ╎│╎   0x00401818      48898d08ffff.  mov qword [option], rcx
│    ┌────< 0x0040181f      0f8788000000   ja 0x4018ad
```

Also, `option` was decremented. (added -1)

### Again

Sends an error message to the client and returns to __Recieve__.

### Gate

```gdb
│    │╎│╎   0x00401825      488b8508ffff.  mov rax, qword [option]
│    │╎│╎   0x0040182c      488b0cc50820.  mov rcx, qword [rax*8 + 0x402008]
│    │╎│╎   0x00401834      ffe1           jmp rcx
```

This code basically uses the memory at `0x402008` as a jump table, and `option` serves as an index into the table.

Let's print 6\*8 bytes at that address as quadwords.
The addresses of the procedures are:
```gdb
0x0000000000401836
0x000000000040184c
0x0000000000401862
0x0000000000401877
0x0000000000401881
0x0000000000401897
```

Upon inspection, we see that the only ones that are useful are the ones used for `!password` and `!flag` commands.
(`0x401862` and `0x401877`)

## What we know so far

The server binds a tcp/ip4 socket to port 13337 and starts accepting connections. Then, it recieves up to 100 bytes and searches it for several commands. Based on these commands, the code jumps to a procedure pointed to by the jump table.

## Continuation of the analysis

### !password

`buffer` and another buffer that is used for sending messages back to the client, let's call it `dest`, are passed to a function at `0x4013c0`, let's call it `fcn.check_pass`.

The contents of `buffer` are split by spaces, using strtok(), and the address of the second part is saved.
```gdb
│           0x004013c8      48897df0       mov qword [s1], rdi         ; arg1
│           0x004013cc      488975e8       mov qword [dest], rsi       ; arg2
│           0x004013d0      488b7df0       mov rdi, qword [s1]         ; char *s1
│           0x004013d4      48be38204000.  movabs rsi, 0x402038        ; '8 @' ; " " ; const char *s2
│           0x004013de      e8cdfcffff     call sym.imp.strtok         ; char *strtok(char *s1, const char *s2)
│           0x004013e3      488945e0       mov qword [src], rax
│           0x004013e7      488b7df0       mov rdi, qword [s1]         ; char *s1
│           0x004013eb      48be38204000.  movabs rsi, 0x402038        ; '8 @' ; " " ; const char *s2
│           0x004013f5      e8b6fcffff     call sym.imp.strtok         ; char *strtok(char *s1, const char *s2)
```

This would be our password.

The password is checked, if its length is 32.
```gdb
│           0x004013fa      488945e0       mov qword [src], rax
│           0x004013fe      488b7de0       mov rdi, qword [src]        ; const char *s
│           0x00401402      e849fcffff     call sym.imp.strlen         ; size_t strlen(const char *s)
│           0x00401407      4883f820       cmp rax, 0x20               ; 32
```

If not, the check has failed. Otherwise, the password is passed to a function at `0x401330`. Let's call this function `fcn.auth`.
```gdb
│      │    0x0040143f      488b7da8       mov rdi, qword [pass]       ; int64_t arg1
│      │    0x00401443      488945a0       mov qword [var_60h], rax
│      │    0x00401447      e8e4feffff     call fcn.auth
```

The password is passed to 3 functions that I called `fcn.f1`, `fcn.f2` and `fcn.f3`.

```gdb
│           0x0040133c      488b7df0       mov rdi, qword [pass]       ; char *arg1
│           0x00401340      e89bfeffff     call fcn.f1
│           0x00401345      488b7df0       mov rdi, qword [pass]       ; char *arg1
│           0x00401349      8945e8         mov dword [var_18h], eax
│           0x0040134c      e8fffeffff     call fcn.f2
│           0x00401351      488b7df0       mov rdi, qword [pass]       ; int64_t arg1
│           0x00401355      8945e4         mov dword [var_1ch], eax
│           0x00401358      e863ffffff     call fcn.f3
```

Let's look at these functions.

#### f1:

This function loops through the password.
```gdb
│           0x004011ec      c745bc000000.  mov dword [index], 0
│       ┌─> 0x004011f3      837dbc20       cmp dword [index], 0x20
```

and performs this block of code on every byte of the password:

```gdb
│      │╎   0x004011fd      488b45f0       mov rax, qword [pass]
│      │╎   0x00401201      48634dbc       movsxd rcx, dword [index]
│      │╎   0x00401205      0fb6140dc040.  movzx edx, byte [rcx + 0x4040c0]
│      │╎   0x0040120d      89d1           mov ecx, edx
│      │╎   0x0040120f      408a3408       mov sil, byte [rax + rcx]
│      │╎   0x00401213      486345bc       movsxd rax, dword [index]
│      │╎   0x00401217      40887405c0     mov byte [rbp + rax - 0x40], sil
```

As we can see, the password address is copied to `rax`, and the index to `rcx`.
Then, it takes a byte from a global variable at `0x4040c0` (indexed by `rcx`) and saves it to `ecx`.

This byte is then used as an index into the password, which is stored into `new_pass` (`rbp-0x40`)

What this effectively does, is shuffling the bytes in the password, using the bytes at `0x4040c0` as new indexes, to order the bytes. This is basically to how the `pshufb` instruction (packed shuffle bytes) works.

Let's look at the bytes at `0x4040c0`
```gdb
0x004040c0  0d02 0b13 1b09 0a00 1006 071a 0512 0419  ................
0x004040d0  110e 1716 0f1c 1d18 0815 0103 1f0c 1e14  ................
```

We will need these to reverse the check process.

The `new_pass` is then copied to the password.
```gdb
│      └──> 0x0040122a      488d75c0       lea rsi, [new_pass]         ; const char *src
│           0x0040122e      c645e000       mov byte [var_20h], 0
│           0x00401232      488b7df0       mov rdi, qword [pass]       ; char *dest
│           0x00401236      e805feffff     call sym.imp.strcpy         ; char *strcpy(char *dest, const char *src)
```

#### f2:

This function is very similar, except this block is performed on every byte of the password:
```gdb
│      │╎   0x0040126d      488b45f0       mov rax, qword [pass]
│      │╎   0x00401271      48634dbc       movsxd rcx, dword [index]
│      │╎   0x00401275      480fbe0408     movsx rax, byte [rax + rcx]
│      │╎   0x0040127a      8a1405e04040.  mov dl, byte [rax + 0x4040e0]
│      │╎   0x00401281      486345bc       movsxd rax, dword [index]
│      │╎   0x00401285      885405c0       mov byte [rbp + rax - 0x40], dl
```

Here, the bytes in the password are used as indexes into a global variable at `0x4040e0`.
The bytes at this address (indexed by the bytes in the password) will be the `new_pass` bytes.

Let's print 256 bytes at `0x4040e0`:
```gdb
0x004040e0  a85f 43df 9015 a2f5 7748 496c 6720 0ecd  ._C.....wHIlg ..
0x004040f0  b6c8 4ae7 892f a1a6 e8b7 e1c6 58a9 d45a  ..J../......X..Z
0x00404100  4d9e 3405 53c2 76d3 c5b3 bfc9 af98 2568  M.4.S.v.......%h
0x00404110  d92d e665 d759 d60a 318f 99aa 7cc0 35b5  .-.e.Y..1...|.5.
0x00404120  ed4b ebd5 8e6b 9d37 2e62 0f07 9b87 b8bd  .K...k.7.b......
0x00404130  de69 c7cf 6646 6004 d0a7 f870 7efa 9a03  .i..fF`....p~...
0x00404140  08c4 f68b 7933 23dd dac1 13ce 16ee 9363  ....y3#........c
0x00404150  126f 830d 7164 4c51 00ba ef95 6e22 e594  .o..qdLQ....n"..
0x00404160  30fb 1441 7a1c 2a56 b938 42f0 44f3 f29f  0..Az.*V.8B.D...
0x00404170  524e d8cb 2432 be0c a309 8501 1da5 2845  RN..$2........(E
0x00404180  f447 ccae c3ab a092 7257 ac3e e3b4 741b  .G......rW.>..t.
0x00404190  814f dc2b 5002 27b2 6df1 54fe 805e 3b36  .O.+P.'.m.T..^;6
0x004041a0  e2ff 11ea fd1a 9786 2673 b1d2 3a1e 5d39  ........&s..:.]9
0x004041b0  7f1f a491 5c55 ece4 298c f77d 1882 bc2c  ....\U..)..}...,
0x004041c0  7540 bb17 8df9 d1e9 0b7b 10ca 6afc 193c  u@.......{..j..<
0x004041d0  8ab0 ad21 965b 0661 3d3f 8878 db84 9ce0  ...!.[.a=?.x....
```

We will need this entire chunk of memory in order to reverse the check.

`new_pass` is again copied to the password.

#### f3:

The third function is again very similar, but the bytes are altered in-place. (instead of saving to a new buffer)

Let's look at the block of code in steps

First, we save the address of the password to `rax`, and the index to `rcx`. We get the indexed byte to `edx`, and save it to `cur_byte`. (`rbp-0x18`) The index is also stored to `eax`:
```gdb
│      │╎   0x004012d9      488b45f0       mov rax, qword [pass]
│      │╎   0x004012dd      48634dec       movsxd rcx, dword [index]
│      │╎   0x004012e1      0fbe1408       movsx edx, byte [rax + rcx]
│      │╎   0x004012e5      8b45ec         mov eax, dword [index]
│      │╎   0x004012e8      8955e8         mov dword [cur_byte], edx
```

Now, we divide the index (in `eax`) by 4, and save the remainder into `rcx`:
```gdb
│      │╎   0x004012eb      99             cdq
│      │╎   0x004012ec      be04000000     mov esi, 4
│      │╎   0x004012f1      f7fe           idiv esi
│      │╎   0x004012f3      4863ca         movsxd rcx, edx
```

This remainder serves as an index into a string literal at `0x4040b0`, named `str.B3_h` and the `cur_byte` is XORed by it:
```gdb
│      │╎   0x004012f6      0fb6140db040.  movzx edx, byte [rcx + str.B3_h] ; [0x4040b0:1]=66 ; "B3!h"
│      │╎   0x004012fe      8b75e8         mov esi, dword [cur_byte]
│      │╎   0x00401301      31d6           xor esi, edx
```

This byte then replaces the current one in the password.
```gdb
│      │╎   0x00401303      488b4df0       mov rcx, qword [pass]
│      │╎   0x00401307      48637dec       movsxd rdi, dword [index]
│      │╎   0x0040130b      40883439       mov byte [rcx + rdi], sil
```

The whole function could be decompiled as:
```C
for(int i = 0; i < 32; i++)
{
	pass[i] ^= B_3h[i % 4]
	// Or pass[i] = pass[i] ^ B_3h[i % 4]
}

return;
```

---

Now, we're back in `fcn.auth`.

This final form of password is then checked against a string literal at the address `0x404090`:
```gdb
│           0x0040135d      c745ec000000.  mov dword [var_14h], 0
│           ; CODE XREF from fcn.auth @ 0x4013a8
│       ┌─> 0x00401364      837dec20       cmp dword [var_14h], 0x20
│      ┌──< 0x00401368      0f8d3f000000   jge 0x4013ad
│      │╎   0x0040136e      488b45f0       mov rax, qword [pass]
│      │╎   0x00401372      48634dec       movsxd rcx, dword [var_14h]
│      │╎   0x00401376      0fbe1408       movsx edx, byte [rax + rcx]
│      │╎   0x0040137a      486345ec       movsxd rax, dword [var_14h]
│      │╎   0x0040137e      0fb634059040.  movzx esi, byte [rax + str.P_PDeO_D_rA] ; [0x404090:1]=80 ; "P!P\ub1b0DeO>D\rA\xea\xa2\xeb\x13\xe4\xb2\fO\xfd\xf6\x9e\xc90E\rT0\xd7\x11B3!h"
│      │╎   0x00401386      39f2           cmp edx, esi
│     ┌───< 0x00401388      0f840c000000   je 0x40139a
```

`0x404090`:
```gdb
0x00404090  5021 50eb 86b0 4465 4f3e 440d 41ea a2eb  P!P...DeO>D.A...
0x004040a0  13e4 b20c 4ffd f69e c930 450d 5430 d711  ....O....0E.T0..
```

If it fails, the password check failed.

---

Now, we are back at `fcn.check_pass`.

If the authentication succeeded, the original password is saved to a global variable at `0x404220`. Let's call it `org_pass`.
Also, a global variable at `0x404201` is set to 1. Let's call it `authenticated`.
```gdb
│     ││└─> 0x0040146f      488b75e0       mov rsi, qword [src]        ; const char *src
│     ││    0x00401473      bf20424000     mov edi, 0x404220           ; ' B@' ; char *dest
│     ││    0x00401478      e8c3fbffff     call sym.imp.strcpy         ; char *strcpy(char *dest, const char *src)
│     ││    0x0040147d      c60425014240.  mov byte [0x404201], 1      ; [0x404201:1]=0
```

### !flag

A single function at `0x4014a0` is called, with no arguments. Let's call it `fcn.get_flag`.

`fcn.get_flag` first checks, if `authenticated` is set to 1 (true):
```gdb
│           0x004014a8      f60425014240.  test byte [0x404201], 1     ; [0x404201:1]=0
│       ┌─< 0x004014b0      0f8451000000   je 0x401507
```

If not, immediately return.

Else, loop through `org_pass`:
```gdb
│       │   0x004014b6      c745f8000000.  mov dword [index], 0
│       │   ; CODE XREF from fcn.get_flag @ 0x4014fd
│      ┌──> 0x004014bd      837df820       cmp dword [index], 0x20
│     ┌───< 0x004014c1      0f8d3b000000   jge 0x401502
```

Get a character, and XOR it with a character at `0x4041e0`. (indexed):
```gdb
│     │╎│   0x004014c7      486345f8       movsxd rax, dword [index]
│     │╎│   0x004014cb      0fbe0c052042.  movsx ecx, byte [rax + 0x404220]
│     │╎│   0x004014d3      486345f8       movsxd rax, dword [index]
│     │╎│   0x004014d7      0fb61405e041.  movzx edx, byte [rax + 0x4041e0]
│     │╎│   0x004014df      31d1           xor ecx, edx
```

the bytes at `0x4041e0` are:
```gdb
0x004041e0  0302 1e56 1624 475f 0e42 4000 001c 0518  ...V.$G_.B@.....
0x004041f0  4028 5610 3b32 461d 1141 5d06 5846 4311  @(V.;2F..A].XFC.
```

Then, print the character:
```gdb
│     │╎│   0x004014e1      48bf71204000.  movabs rdi, 0x402071        ; 'q @' ; "%c" ; const char *format
│     │╎│   0x004014eb      89ce           mov esi, ecx
│     │╎│   0x004014ed      b000           mov al, 0
│     │╎│   0x004014ef      e88cfbffff     call sym.imp.printf         ; int printf(const char *format)
```

## Solution

Now, we pretty much know the authentication method and the flag decryption method. We can attempt to reverse it.

I have included the solution in `shuffle.py`.

### f1\_solve()

Let's start at the first function.

We will need the bytes by which the password is shuffled:
```py
f1_orders = [0x0d, 0x02, 0x0b, 0x13, 0x1b, 0x09, 0x0a, 0x00, 0x10, 0x06, 0x07, 0x1a, 0x05, 0x12, 0x04, 0x19, 0x11, 0x0e, 0x17, 0x16, 0x0f, 0x1c, 0x1d, 0x18, 0x08, 0x15, 0x01, 0x03, 0x1f, 0x0c, 0x1e, 0x14]
```

We will loop through all the indexes (0-31) and we will search for a byte in `f1_solve` that equals to the index.
We will append a byte from the password at that position to the new password.
```py
def f1_solve(passwd):
    new_passwd = []

    for ind in range(0, len(f1_orders)):
        for on in range(0, len(f1_orders)):
            if f1_orders[on] == ind:
                new_passwd.append(passwd[on])
                break

    return new_passwd
```

### f2\_solve()

We will need the chunk of memory:
```py
f2_offset_mem = [0xa8, 0x5f, 0x43, 0xdf, 0x90, 0x15, 0xa2, 0xf5, 0x77, 0x48, 0x49, 0x6c, 0x67, 0x20, 0x0e, 0xcd, 0xb6, 0xc8, 0x4a, 0xe7, 0x89, 0x2f, 0xa1, 0xa6, 0xe8, 0xb7, 0xe1, 0xc6, 0x58, 0xa9, 0xd4, 0x5a, 0x4d, 0x9e, 0x34, 0x05, 0x53, 0xc2, 0x76, 0xd3, 0xc5, 0xb3, 0xbf, 0xc9, 0xaf, 0x98, 0x25, 0x68, 0xd9, 0x2d, 0xe6, 0x65, 0xd7, 0x59, 0xd6, 0x0a, 0x31, 0x8f, 0x99, 0xaa, 0x7c, 0xc0, 0x35, 0xb5, 0xed, 0x4b, 0xeb, 0xd5, 0x8e, 0x6b, 0x9d, 0x37, 0x2e, 0x62, 0x0f, 0x07, 0x9b, 0x87, 0xb8, 0xbd, 0xde, 0x69, 0xc7, 0xcf, 0x66, 0x46, 0x60, 0x04, 0xd0, 0xa7, 0xf8, 0x70, 0x7e, 0xfa, 0x9a, 0x03, 0x08, 0xc4, 0xf6, 0x8b, 0x79, 0x33, 0x23, 0xdd, 0xda, 0xc1, 0x13, 0xce, 0x16, 0xee, 0x93, 0x63, 0x12, 0x6f, 0x83, 0x0d, 0x71, 0x64, 0x4c, 0x51, 0x00, 0xba, 0xef, 0x95, 0x6e, 0x22, 0xe5, 0x94, 0x30, 0xfb, 0x14, 0x41, 0x7a, 0x1c, 0x2a, 0x56, 0xb9, 0x38, 0x42, 0xf0, 0x44, 0xf3, 0xf2, 0x9f, 0x52, 0x4e, 0xd8, 0xcb, 0x24, 0x32, 0xbe, 0x0c, 0xa3, 0x09, 0x85, 0x01, 0x1d, 0xa5, 0x28, 0x45, 0xf4, 0x47, 0xcc, 0xae, 0xc3, 0xab, 0xa0, 0x92, 0x72, 0x57, 0xac, 0x3e, 0xe3, 0xb4, 0x74, 0x1b, 0x81, 0x4f, 0xdc, 0x2b, 0x50, 0x02, 0x27, 0xb2, 0x6d, 0xf1, 0x54, 0xfe, 0x80, 0x5e, 0x3b, 0x36, 0xe2, 0xff, 0x11, 0xea, 0xfd, 0x1a, 0x97, 0x86, 0x26, 0x73, 0xb1, 0xd2, 0x3a, 0x1e, 0x5d, 0x39, 0x7f, 0x1f, 0xa4, 0x91, 0x5c, 0x55, 0xec, 0xe4, 0x29, 0x8c, 0xf7, 0x7d, 0x18, 0x82, 0xbc, 0x2c, 0x75, 0x40, 0xbb, 0x17, 0x8d, 0xf9, 0xd1, 0xe9, 0x0b, 0x7b, 0x10, 0xca, 0x6a, 0xfc, 0x19, 0x3c, 0x8a, 0xb0, 0xad, 0x21, 0x96, 0x5b, 0x06, 0x61, 0x3d, 0x3f, 0x88, 0x78, 0xdb, 0x84, 0x9c, 0xe0]
```

We will go through every byte in the password, and search, at which index the byte is located in the chunk.
We will save the index as the new byte of the password.
```py
def f2_solve(passwd):
    new_passwd = []

    for pb in passwd:
        for mm in range(0, len(f2_offset_mem)):
            if pb == f2_offset_mem[mm]:
                new_passwd.append(mm)
                break

    return new_passwd
```

### f3\_solve()

We will need the 4 bytes of the string literal:
```py
f3_bytes = [0x42, 0x33, 0x21, 0x68]
```

XOR is reversible, so we will simply repeat the same process.
Get the `f3_bytes[index % 4]`, and XOR that with the current byte.
```py
def f3_solve(passwd):
    new_passwd = []

    for i in range(0, len(passwd)):
        ind_mod = f3_bytes[i % 4]
        new_passwd.append(ind_mod ^ passwd[i])

    return new_passwd
```

### Complete the solution

Now, we need the base that the modified password was compared to:
```py
base = [0x50, 0x21, 0x50, 0xeb, 0x86, 0xb0, 0x44, 0x65, 0x4f, 0x3e, 0x44, 0x0d, 0x41, 0xea, 0xa2, 0xeb, 0x13, 0xe4, 0xb2, 0x0c, 0x4f, 0xfd, 0xf6, 0x9e, 0xc9, 0x30, 0x45, 0x0d, 0x54, 0x30, 0xd7, 0x11]
```

And the resource against which the original password was XORed, to decrypt the flag.
```py
flag_resource = [0x03, 0x02, 0x1e, 0x56, 0x16, 0x24, 0x47, 0x5f, 0x0e, 0x42, 0x40, 0x00, 0x00, 0x1c, 0x05, 0x18, 0x40, 0x28, 0x56, 0x10, 0x3b, 0x32, 0x46, 0x1d, 0x11, 0x41, 0x5d, 0x06, 0x58, 0x46, 0x43, 0x11]
```

The function to decrypt the flag:
```py
def get_flag(passwd):
    flag = []

    for i in range(0, len(flag_resource)):
        flag.append(flag_resource[i] ^ passwd[i])

    return flag
```

And we can apply the functions in the reverse order.
```py
get_flag(f1_solve(f2_solve(f3_solve(base))))
```

We just need to convert the integers to characters:
```py
def arr_to_str(arr):
    string = ""

    for i in arr:
        string += chr(i)

    return string
```

Final print statement:
```py
print(arr_to_str(get_flag(f1_solve(f2_solve(f3_solve(base))))))
```

And, we have the flag: `pwned{4lm0st_l1k3_fb_m3ss3ng3r!}`
